# For more info: https://developers.google.com/resources/api-libraries/documentation/customsearch/v1/python/latest/customsearch_v1.cse.html#list
# To get yours Google APIs Key, enter: <http://code.google.com/apis/console>
# To get your Custom Search Engine Key, enter <https://cse.google.com/cse/all>

import json
from googleapiclient.discovery import build
from optparse import OptionParser
# from credentials import *
import csv


class GoogleSearch():
    
    def __init__(self):
        parser = OptionParser()
        parser.add_option("-s", "--input_csv", default="./example/import_example.csv",
                          action="store", dest="input_csv",
                          help="path to your input csv file")
        
        parser.add_option("-n", "--number_of_pages", default=10,
                          action="store", dest="page_limit",
                          help="number of pages to scrape")
        
        (options, args) = parser.parse_args()
        
        if not options.input_csv:
            parser.error('Missing input csv')
            
        if int(options.page_limit) > 10:
            parser.error('Google API can only returns 10 pages')
    
        self.opts = (options, args)
        self.service = None
        self.getCredentials()
        self.getService()
        self.main()


    def getService(self):
        self.service = build("customsearch", "v1",
                developerKey=self.api_key)
        
        
    def getCredentials(self):
        with open('./credentials/credentials.csv', 'r') as infile:
            csv_reader = csv.reader(infile)
            for row in csv_reader:
                setattr(self, row[0], row[1])


    def search(self, query):
    
        startIndex = 1
        response = []
    
        for nPage in range(0, int(self.opts[0].page_limit)):
            print("Reading page number:", nPage+1)
    
            response.append(self.service.cse().list(
                q=query, #Search words
                cx=self.search_engine_id,  #CSE Key
                lr='lang_en', #Search language
                start=startIndex
            ).execute())
        
            startIndex = response[nPage].get("queries").get("nextPage")[0].get("startIndex")
    
        with open('data.json', 'w') as outfile:
            json.dump(response, outfile)
        
        return response
            
            
    def main(self):
        
        with open('./output/output_data.csv', 'w', newline='') as outfile:
            csv_writer = csv.writer(outfile)
            csv_writer.writerow(['Lead ID', 'Phone Number', 'Search Phrase', 'Website', 'Google Ranking'])
            with open(self.opts[0].input_csv, 'r') as infile:
                csv_reader = csv.reader(infile)
                first_row = True
                for row in csv_reader:
                    if first_row:
                        first_row = False
                    else:
                        query = row[2]
                        lead_id = row[0]
                        phone_number = row[1]
                        response = self.search(query)
                        i = 0
                        for idx in range(len(response)):
                            page = response[idx]
                            for item in page['items']:
                                csv_writer.writerow([lead_id, phone_number, query, item['link'], i])
                                i += 1


if __name__ == '__main__':
    item_pd = GoogleSearch()