# Google-Search

# Initial set-up (very important!):
- Go to https://cse.google.com/cse/all to create a Custom Google Search Engine
- Click "Add" -> name the search engine
- In "Sites to search", put `*.com/*`
- Turn on "Search the entire web"
- Record the "Search engine ID"
- Create your API key here: https://cloud.google.com/docs/authentication/api-keys
- Enter your "API key" and "Search engine ID" into /credentials/credentials.csv
 
# To run the script:
- Make sure Python is installed on your computer
- Use CMD to navigate to project's folder: use can use command `cd` (for example: `cd d:\google_search\`)
- Run `pip install -r requirements.txt`
- After installation is complete, run `python google-search.py -s <path_to_input_csv> -n <number_of_page - maximum 10>` (for example: `python google-search.py -s ./example/import_example.csv -n 4`)
- In the script, `-n` is default to be 10

# To run the script as exe:
- Use CMD to navigate to project's folder: use can use command `cd` (for example: `cd d:\google_search\`)
- run `google-search -s <path_to_input_csv> -n <number_of_page - maximum 10>` (for example: `google-search -s ./example/import_example.csv -n 4`)
- In the script, `-n` is default to be 10
